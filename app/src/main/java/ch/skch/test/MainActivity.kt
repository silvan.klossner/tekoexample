package ch.skch.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import ch.skch.test.model.networking.Joke
import ch.skch.test.networking.MyOwnRetrofitClient


import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "HUBABUBA"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Bind navigation component with tab bar
        val navController = findNavController(R.id.nav_host_fragment)
        bottom_nav.setupWithNavController(navController)

        //Get the client
        val client = MyOwnRetrofitClient()
        //Now start the webcall
        client.apiClient.fetchRandomJoke()
            .enqueue(object: Callback<Joke> {
                override fun onFailure(call: Call<Joke>, t: Throwable) {

                }

                override fun onResponse(call: Call<Joke>, response: Response<Joke>) {
                    if (response.isSuccessful) {
                        Log.d("Answer", response.body()?.value ?: "")
                    } else {
                        //TODO something went wrong check what is is.
                    }
                }
            })


    }


}
