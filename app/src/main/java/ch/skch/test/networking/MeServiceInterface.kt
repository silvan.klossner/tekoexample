package ch.skch.test.networking

import ch.skch.test.model.networking.Joke
import retrofit2.Call
import retrofit2.http.GET

/**
 * This interface will represent our API.
 */
interface MeServiceInterface {

    @GET("jokes/random")
    fun fetchRandomJoke(): Call<Joke>

}