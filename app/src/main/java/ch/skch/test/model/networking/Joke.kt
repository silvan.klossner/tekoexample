package ch.skch.test.model.networking

import com.google.gson.annotations.SerializedName

data class Joke (
    @SerializedName("icon_url")
    val iconUrl: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("url")
    val url: String = "",
    @SerializedName("value")
    val value: String = ""
)