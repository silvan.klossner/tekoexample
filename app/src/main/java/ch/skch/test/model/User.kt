package ch.skch.test.model

data class User(
    val age: Int,
    val firstName: String,
    val lastName: String
)