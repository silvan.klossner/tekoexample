package ch.skch.test.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import ch.skch.test.R
import ch.skch.test.adapter.UserAdapter
import ch.skch.test.model.User
import kotlinx.android.synthetic.main.fragment_list.*


class ListFragment : Fragment() {

    //Create an instance of the adapter that we want to use
    var adapter = UserAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //Create a dummy list of data
        val data: MutableList<User> = arrayListOf()
        for (i in 0..33) {
            data.add(User(3, "Hansli", "Ueli"))
            data.add(User(34, "Fredi", "Ramseier"))
            data.add(User(26, "Freni", "Hubler"))
        }


        //Inform the recycler view that it uses the adapter we created.
        recycler_view.adapter = adapter
        //The layout manager will tell the recycler view HOW to render the rows.
        recycler_view.layoutManager = LinearLayoutManager(view.context)
        //Set the data
        adapter.setData(data)

    }


}
