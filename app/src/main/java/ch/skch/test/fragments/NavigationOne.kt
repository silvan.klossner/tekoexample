package ch.skch.test.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ch.skch.test.helper.Constants


class NavigationOne : Fragment() {

    companion object {
        const val FILE_NAME = "mySharedPrefs"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(ch.skch.test.R.layout.fragment_navigation_one, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        //Write
        view.context
            .getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
            .edit()
            .putString(Constants.KEY_IS_FAVOURITE, "")
            .commit()


        //Read
        val isFav = view.context
            .getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
            .getString(Constants.KEY_IS_FAVOURITE, "Hey this is a favourite")

    }
}
